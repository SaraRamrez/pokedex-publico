// script.js
let pokemonList = [];

// Descargar la lista completa de Pokémon al cargar la página
fetch("https://pokeapi.co/api/v2/pokemon?limit=1126")
    .then(response => response.json())
    .then(data => {
        pokemonList = data.results;
    })
    .catch(error => console.error("Error al obtener la lista de Pokémon:", error));

function searchPokemon() {
    const pokemonName = document.getElementById("pokemonName").value.toLowerCase();
    const pokemonDetailsContainer = document.getElementById("pokemonDetails");

    // Limpiar detalles anteriores
    pokemonDetailsContainer.innerHTML = "";

    // Buscar el Pokémon en la lista descargada
    const matchingPokemon = pokemonList.find(pokemon => pokemon.name === pokemonName);

    if (!matchingPokemon) {
        pokemonDetailsContainer.innerHTML = "<p>Pokémon no encontrado</p>";
        return;
    }

    // Realizar solicitud al backend para obtener detalles del Pokémon
    fetch(matchingPokemon.url)
        .then(response => {
            if (!response.ok) {
                throw new Error("Error al obtener detalles del Pokémon");
            }
            return response.json();
        })
        .then(data => {
            // Mostrar detalles del Pokémon en el frontend
            pokemonDetailsContainer.innerHTML = `
                <h2>${data.name}</h2>
                <p>Altura: ${data.height}</p>
                <p>Peso: ${data.weight}</p>
                <p>Puntos de Vida: ${data.stats[0].base_stat}</p>
                <p>Ataque: ${data.stats[1].base_stat}</p>
                <p>Defensa: ${data.stats[2].base_stat}</p>
                <p>Velocidad: ${data.stats[5].base_stat}</p>
                <p>Tipos: ${data.types.map(type => type.type.name).join(", ")}</p>
                <img src="${data.sprites.front_default}" alt="Frontal">
                <img src="${data.sprites.back_default}" alt="Posterior">
            `;
            
            // Mostrar la sección de detalles
            pokemonDetailsContainer.classList.remove("hidden");
        })
        .catch(error => {
            console.error("Error:", error);
            pokemonDetailsContainer.innerHTML = "<p>Error al obtener detalles del Pokémon</p>";
        });
}


