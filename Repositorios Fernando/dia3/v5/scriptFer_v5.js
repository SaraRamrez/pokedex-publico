'use strict'

// Base de datos API
const baseURL = 'https://pokeapi.co/api/v2/pokemon/';
const pokemon = document.getElementById('nombrePokemon');
const pokeLupa = document.getElementById('elBuscador');
const appNode = document.getElementById('buscador');

// Modo oscuro
let toogle = document.getElementById('toggle');
let label_toggle = document.getElementById('label_toggle');
toggle.addEventListener('change',(event)=>{
   let checked = event.target.checked; 
   document.body.classList.toggle('dark');
   if (checked === true){
    const modoNoche = document.createElement('img')
    modoNoche = document.getElementById('umbreonImg')
    modoNoche.innerHTML = "https://i.postimg.cc/26xyLwc8/light-mode2.png";
    document.body.appendChild(modoNoche);
    return checked;
    
   }else{
    document.getElementById('umbreonImg').src="https://i.postimg.cc/26xyLwc8/light-mode2.png"
    return checked;
   }
});

/*
if (checked == true){
    const modoNoche = document.createElement('img')
    modoNoche.src = "https://i.postimg.cc/5tdk9VhW/light-mode.png";
    document.body.appendChild(modoNoche);

    "https://i.postimg.cc/26xyLwc8/light-mode2.png"
    "https://i.postimg.cc/CLHb0GH7/dark-mode2.png"

   }else{
    const modoDia = document.getElementById('espeonImg');
    modoDia.innerHTML = "https://i.postimg.cc/CLHb0GH7/dark-mode2.png";
    document.body.appendChild(modoDia);

---------

    if (checked === true){
    const modoNoche = document.getElementById('umbreonImg');
    modoNoche.innerHTML = "https://i.postimg.cc/CLHb0GH7/dark-mode2.png";
    document.body.appendChild(modoNoche);

   }else{
    const modoDia = document.getElementById('espeonImg');
    modoDia.innerHTML = "https://i.postimg.cc/26xyLwc8/light-mode2.png";
    document.body.appendChild(modoDia);
   }
});
*/


// Recargar página en el logo
const recargar = document.getElementById('encabezado')

recargar.addEventListener('click', recargaPagina)

function recargaPagina () {
    window.location.reload()
}

//Icono lupa hace click 
pokeLupa.addEventListener('click', insertarPokemon)

// Segundo click con enter

document.addEventListener('keyup', function(e) {
    if (e.key === 'Enter' && !e.shiftKey) {
        let boton = document.getElementById('elBuscador');
        boton.click();
    }
});

async function insertarPokemon() {
    try {
        appNode.innerHTML = "";
        const response = await fetch(`${baseURL}${pokemon.value.toLowerCase()}`);
        if (response.status === 404) {
            const mensajeError = document.createElement("p");
            mensajeError.innerText = "Este pokemon no está disponible. ¡Prueba con otro!";
            appNode.appendChild(mensajeError);
        } else {
            const responseJSON = await response.json();
            const todaLaInfo = [];
            const resultado = [];
        for (let pokemonInfo in responseJSON) {
        resultado.push([pokemonInfo, responseJSON[pokemonInfo]]);
        }
    console.table(resultado);

    // Obtenemos la imagen del servidor

    const imagenPokemon = document.createElement('img')
    imagenPokemon.src = resultado[15][1].front_default;

    // Con esta función hacemos que, si no hay imagen trasera no muestre nada
    const imagenPokemon2 = document.createElement("img");
    if (resultado[15][1].back_default) {
        imagenPokemon2.src = resultado[15][1].back_default;
    }

    //imagenPokemon.addEventListener('click', shiny())

    //shiny (){
    // imagenPokemon.src = resultado}

    // Nombre
    const nombrePokemon = document.createElement('p')
    nombrePokemon.innerText = `Nombre: ${resultado[10][1]}`
    // Número de Pokémon
    const numPokemon = document.createElement('p')
    numPokemon.innerText = `Número: ${resultado[6][1]}`

    // Tipo de Pokemon 
    const tipoPokemon1 = document.createElement('p')
    tipoPokemon1.innerText = `Tipo: ${resultado[17][1][0].type.name}`

    const tipoPokemon2 = document.createElement('p');
    if (resultado[17][1][1]) {
        tipoPokemon2.innerText = 
        `Tipo 2: ${resultado[17][1][1].type.name}`;
    }
     
    // Estatura
    const estaturaPokemon = document.createElement('p')
    estaturaPokemon.innerText = `Estatura: ${resultado[4][1]}`

    //Peso
    const pesoPokemon = document.createElement('p')
            pesoPokemon.innerText = `Peso: ${resultado [18][1]}`

    // Puntos de vida
    const vidaPokemon = document.createElement('p')
    vidaPokemon.innerText = ` Vida: ${resultado [16][1][0].base_stat}`

    // Ataque
    const ataquePokemon = document.createElement('p')
    ataquePokemon.innerText = `Ataque: ${resultado [16][1][1].base_stat}`

    // Defensa
    const defensaPokemon = document.createElement('p')
    defensaPokemon.innerText = `Defensa: ${resultado [16][1][2].base_stat}`  

    // Velocidad
    const velocidadPokemon = document.createElement('p')
    velocidadPokemon.innerText = `Velocidad: ${resultado [16][1][3].base_stat}`

    // Contenedor
    const contenedor = document.createElement("div");
    contenedor.append(imagenPokemon, imagenPokemon2, nombrePokemon, numPokemon, tipoPokemon1, tipoPokemon2, estaturaPokemon, pesoPokemon, vidaPokemon, ataquePokemon, defensaPokemon, velocidadPokemon);

    todaLaInfo.push(contenedor);

    appNode.append(...todaLaInfo);
  }

      } catch (error) {
          console.error(error);
}};



    /*
// Contenedor de datos
// Contenedor
    const contenedor = document.createElement("div");
    contenedor.append( imagenPokemon, imagenPokemon2, nombrePokemon, numPokemon, tipoPokemon1, tipoPokemon2, estaturaPokemon, pesoPokemon, vidaPokemon, ataquePokemon, defensaPokemon, velocidadPokemon);

    todaLaInfo.push(contenedor);

    appNode.append(...todaLaInfo);
  }

            
        const buscadorSection = document.getElementById("buscador");
        buscadorSection.createElement("ficha")
        buscadorSection.appendChild(ficha);

        ficha.ClassList.add('tarjeta');
        ficha.appendChild(imagen1)
        ficha.appendChild(imagen2)
        ficha.appendChild(nombre1)
        ficha.appendChild(num2)
        ficha.appendChild(tipo3)
        ficha.appendChild(tipo4)
        ficha.appendChild(altura5)
        ficha.appendChild(peso6)
        ficha.appendChild(vida7)
        ficha.appendChild(ataque8)
        ficha.appendChild(defensa9)
        ficha.appendChild(velocidad10)
        }

    } catch (error) {
        console.error(error);
}};

*/