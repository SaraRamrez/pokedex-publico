const baseURL = 'https://pokeapi.co/api/v2/pokemon/';
const pokemon = document.getElementById('nombrePokemon');
const pokeLupa = document.getElementById('elBuscador');
const appNode = document.getElementById('buscador');
const recargar = document.getElementById('encabezado')

recargar.addEventListener('click', recargaPagina)

function recargaPagina () {
    window.location.reload()
}

pokeLupa.addEventListener('click', insertarPokemon)

// Segundo click con enter
function pulsar (e) {
    if (e.keyCode === 13 && !e.shiftKey) {
        var boton = document.getElementById('elBuscador');
        angular.element('elBuscador').triggerHandler('click');
    }
}

function insertarPokemon (){
    window.fetch(`${baseURL}${pokemon.value.toLocaleLowerCase()}`)
        .then(response => {
            if (response.status === 404) {
                alert('Este pokemon no está disponible. Prueba con otro!')
            } else {
                return response.json()
            }
        })
        .then(responseJSON => {
            const todaLaInfo = []
            const resultado = []
            for(let pokemonInfo in responseJSON){
                resultado.push([pokemonInfo, responseJSON[pokemonInfo]])
            }
            console.table(resultado);

            // Obtenemos la imagen del servidor

            const imagenPokemon = document.createElement('img')
            imagenPokemon.src = resultado[15][1].front_default

            const imagenPokemon2 = document.createElement('img')
            imagenPokemon2.src= resultado[15][1].back_default

            //imagenPokemon.addEventListener('click', shiny())

            //shiny (){
           // imagenPokemon.src = resultado}

            // Nombre
            const nombrePokemon = document.createElement('h2')
            nombrePokemon.innerText = `Nombre: ${resultado[10][1]}`
            // Número de Pokémon
            const numPokemon = document.createElement('h2')
            numPokemon.innerText = `Número: ${resultado[6][1]}`

            // Tipo de Pokemon 
            const tipoPokemon = document.createElement('h2')
            tipoPokemon.innerText = `Tipo: ${resultado[17][1][0].type.name}`

            // Estatura
            const estaturaPokemon = document.createElement('h2')
            estaturaPokemon.innerText = `Estatura: ${resultado[4][1]}`

            //Peso
            const pesoPokemon = document.createElement('h2')
            pesoPokemon.innerText = `Peso: ${resultado [18][1]} g`



            // Contenedor de datos
            const contenedor = document.createElement('div')
            contenedor.append(imagenPokemon, imagenPokemon2, nombrePokemon, numPokemon, tipoPokemon, estaturaPokemon, pesoPokemon)

            todaLaInfo.push(contenedor) 
            
            appNode.append(...todaLaInfo)
        })
}

