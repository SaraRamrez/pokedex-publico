const baseURL = 'https://pokeapi.co/api/v2/pokemon/';
const pokemon = document.getElementById('nombrePokemon');
const botonLupa = document.getElementById('busqueda');
const appNode = document.getElementById('buscador');

botonLupa.addEventListener('click', insertarPokemon)
botonLupa.addEventListener('touchstart', insertarPokemon)

document.getElementById("busqueda").addEventListener('keydown', (e) => {
    if (e.key === "Enter") {
      document.getElementById(insertarPokemon()).click();
    }
  });

function insertarPokemon (){
    window.fetch(`${baseURL}${pokemon.value.toLocaleLowerCase()}`)
        .then(response => {
            if (response.status === 400) {
                alert('Este pokemon no está disponible. Prueba con otro!')
            } else {
                return response.json()
            }
        })
        .then(responseJSON => {
            const todaLaInfo = []
            const resultado = []
            for(let pokemonInfo in responseJSON){
                resultado.push([pokemonInfo, responseJSON[pokemonInfo]])
            }
            console.table(resultado);

            // Obtenemos la imagen del servidor

            const imagenPokemon = document.createElement('img')
            imagenPokemon.src = resultado[15][1].front_default

            // Nombre y número
            
            const nombrePokemon = document.createElement('h2')
            nombrePokemon.innerText = `Nombre: ${resultado[10][1]}- Número: ${resultado[6][1]}`

            // Tipo de Pokemon 
            const tipoPokemon = document.createElement('h2')
            tipoPokemon.innerText = `Tipo: ${resultado[17][1][0].type.name}`

            // Contenedor de datos
            const contenedor = document.createElement('div')
            contenedor.append(imagenPokemon, nombrePokemon, tipoPokemon)

            todaLaInfo.push(contenedor) 
            
            appNode.append(...todaLaInfo)
        })
}

