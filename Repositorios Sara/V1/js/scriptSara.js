'use strict'

// Base de datos API
const baseURL = 'https://pokeapi.co/api/v2/pokemon/';
const pokemon = document.getElementById('nombrePokemon');
const pokeLupa = document.getElementById('elBuscador');
const appNode = document.getElementById('buscador');

// Modo oscuro
let toogle = document.getElementById('toggle');
let label_toggle = document.getElementById('label_toggle');

toggle.addEventListener('change',(event)=>{
    let checked = event.target.checked; 
    document.body.classList.toggle('dark');
    if (checked === true){
    const modoNoche = document.createElement('img');
    modoNoche = document.getElementById('umbreonImg');
    modoNoche.innerHTML = "https://i.postimg.cc/26xyLwc8/light-mode2.png";
    document.body.appendChild(modoNoche);
    return checked;
    
    } else{
    document.getElementById('umbreonImg').src="https://i.postimg.cc/26xyLwc8/light-mode2.png"
    return checked;
    }
});

// Recargar página en el logo
const recargar = document.getElementById('encabezado');
recargar.addEventListener('click', recargaPagina);
function recargaPagina () {
    window.location.reload()
};

//Icono lupa hace click 
pokeLupa.addEventListener('click', insertarPokemon);

// Segundo click con enter
document.addEventListener('keyup', function(e) {
    if (e.key === 'Enter' && !e.shiftKey) {
        let boton = document.getElementById('elBuscador');
        boton.click();
    }
});

// Función donde obtenemos los datos de la API.
async function insertarPokemon() {
    try {
        // Limpiamos el contenido del elemento con id 'appNode'
        appNode.innerHTML = "";

        // Realizamos una solicitud a la API usando el nombre del Pokémon ingresado
        const response = await fetch(`${baseURL}${pokemon.value.toLowerCase()}`);

        // Verificamos si la respuesta indica que el Pokémon no se encontró (código de estado 404)
        if (response.status === 404) {
            // Creamos un mensaje de error y lo agregamos al DOM
            const mensajeError = document.createElement("p");
            mensajeError.innerText = "Este Pokémon no está disponible. ¡Prueba con otro!";
            appNode.appendChild(mensajeError);
        } else {
            // Convertimos la respuesta a formato JSON
            const responseJSON = await response.json();

            // Inicializamos arrays para almacenar la información del Pokémon
            const todaLaInfo = [];
            const resultado = [];

            // Recorremos el objeto JSON y almacenamos la información en el array 'resultado'
            for (let pokemonInfo in responseJSON) {
                resultado.push([pokemonInfo, responseJSON[pokemonInfo]]);
            }

            // Mostramos la información en formato de tabla en la consola
            console.table(resultado);

            // Creamos elementos del DOM para mostrar la información del Pokémon
            const imagenPokemon = document.createElement('img');
            imagenPokemon.src = resultado[15][1].front_default;

            const imagenPokemon2 = document.createElement("img");
            // Verificamos si hay una imagen trasera y la asignamos a 'imagenPokemon2'
            if (resultado[15][1].back_default) {
                imagenPokemon2.src = resultado[15][1].back_default;
            }

            // Creación de elementos para mostrar el nombre, número, tipo, estatura, peso, etc.
            const nombrePokemon = document.createElement('p');
            nombrePokemon.innerText = `Nombre: ${resultado[10][1]}`;

            const numPokemon = document.createElement('p');
            numPokemon.innerText = `Número: ${resultado[6][1]}`;

            const tipoPokemon1 = document.createElement('p');
            tipoPokemon1.innerText = `Tipo: ${resultado[17][1][0].type.name}`;

            const tipoPokemon2 = document.createElement('p');
            // Verificamos si hay un segundo tipo y lo mostramos
            if (resultado[17][1][1]) {
                tipoPokemon2.innerText = `Tipo 2: ${resultado[17][1][1].type.name}`;
            }

            const estaturaPokemon = document.createElement('p');
            estaturaPokemon.innerText = `Estatura: ${resultado[4][1]}`;

            const pesoPokemon = document.createElement('p');
            pesoPokemon.innerText = `Peso: ${resultado[18][1]}`;

            const vidaPokemon = document.createElement('p');
            vidaPokemon.innerText = ` Vida: ${resultado[16][1][0].base_stat}`;

            const ataquePokemon = document.createElement('p');
            ataquePokemon.innerText = `Ataque: ${resultado[16][1][1].base_stat}`;

            const defensaPokemon = document.createElement('p');
            defensaPokemon.innerText = `Defensa: ${resultado[16][1][2].base_stat}`;

            const velocidadPokemon = document.createElement('p');
            velocidadPokemon.innerText = `Velocidad: ${resultado[16][1][3].base_stat}`;

            // Creamos un contenedor y agregamos todos los elementos creados
            const contenedor = document.createElement("div");
            contenedor.append(nombrePokemon, numPokemon, tipoPokemon1, tipoPokemon2, estaturaPokemon, pesoPokemon, vidaPokemon, ataquePokemon, defensaPokemon, velocidadPokemon, imagenPokemon, imagenPokemon2);

            // Agregamos el contenedor al elemento con id 'appNode'
            todaLaInfo.push(contenedor);
            appNode.append(...todaLaInfo);
        }
    } catch (error) {
        // Capturamos y mostramos cualquier error en la consola
        console.error(error);
    }
};



